jQuery(document).ready(function ($) {
    $('#nk-login-form').submit(function (e) {
        e.preventDefault();
        var username = $('#username').val();
        var password = $('#password').val();
        var data = {
            'username': username,
            'password': password
        };

        if(password === '' || username === ''){
            $("#error-message")
                .css({
                    'display' : 'block',
                    'color'   : '#de6060'
                })
                .text('All fields are required!')
                .fadeIn("slow");
        }
        else {
            $.ajax( {
                url: nkLoginApi.url,
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', nkLoginApi.nonce );
                    $('#loading-animation').css('display', 'block');
                    $('#error-message').css('display', 'none');
                    $('#login-btn').attr('disabled', 'disabled');
                },
                body: data
            } ).done( function ( response ) {
                $('#login-btn').removeAttr('disabled');
                if(response.status === "BLOCKED"){
                    $('#loading-animation').css('display', 'none');
                    $("#error-message")
                        .css({
                            'display' : 'block',
                            'color'   : '#de6060'
                        })
                        .text(response.message)
                        .fadeIn("slow");
                }
                else {
                    // console.log(response);
                    $('#loading-animation').css('display', 'none');
                    $('#error-message')
                        .css({
                            'display' : 'block',
                            'color'   : '#4caf50'
                        })
                        .text('Success')
                        .fadeOut( "slow" );

                    location.reload();
                }
            } ).fail(function() {
                $('#login-btn').removeAttr('disabled');
                $('#loading-animation').css('display', 'none');
                $("#error-message")
                    .css({
                        'display' : 'block',
                        'color'   : '#de6060'
                    })
                    .text('Invalid login details')
                    .fadeIn("fast");
            });
        }
    });
});
