<?php

//Fired during plugin deactivation

class Nk_Login_Activator {

    public static function activate() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'nk_invalid_login_attempts';
        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
              $charset_collate = $wpdb->get_charset_collate();
              $sql = "CREATE TABLE $table_name (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              ip_address VARCHAR( 20 ) NOT NULL,
              username VARCHAR( 255 ) NOT NULL,
              attempts INT NOT NULL,
              last_attempt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              blocked_till datetime NULL,
              PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
    }

}
