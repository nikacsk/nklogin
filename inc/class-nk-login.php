<?php
// Main plugin Logic

class Nk_Login {

    public function __construct()
    {
        add_action('wp_enqueue_scripts', array($this, 'load_nk_login_assets'), 0 );
        add_action( 'admin_menu', array($this, 'nk_admin_menu'), 0 );
        add_action('admin_enqueue_scripts', array($this, 'load_nk_login_backend_assets'), 0 );
        add_action('rest_api_init', array($this, 'register_nk_login_route'), 0);
    }

    // Loading styles and scripts
    public function load_nk_login_assets() {
        wp_register_style( 'nk-login-style', __NKLOGINURL__ . '/assets/frontend/css/nk-style.css' );
        wp_deregister_script( 'jquery' );
        wp_register_script( 'jquery','http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js', array(), '20151215', true );
        wp_register_script( 'nk-login-script', __NKLOGINURL__ . '/assets/frontend/js/nk-scripts.js', array(), '20151215', true );

        // enqueue assets
        wp_enqueue_script('jquery');
        wp_enqueue_script('nk-login-script');
        wp_enqueue_style('nk-login-style');

        wp_localize_script( 'nk-login-script', 'nkLoginApi', array(
            'url' => '/wp-json/nk-login/v1/login/?',
            'nonce' => wp_create_nonce( 'wp_rest' )
        ) );
    }

    // Loading backend styles and scripts
    public function load_nk_login_backend_assets() {
        wp_register_style( 'nk-login-admin-style', __NKLOGINURL__ . '/assets/backend/css/nk-style.css' );
        wp_register_script( 'labs-custom-admin-script', __NKLOGINURL__ . '/assets/backend/js/nk-scripts.js', array(), '20151215', true );

        // enqueue assets
        wp_enqueue_script('nk-login-script');
        wp_enqueue_style('nk-login-style');
    }

    //Add admin page
    public function nk_admin_menu() {
        add_menu_page( 'NK Login Form', 'Login Form', 'manage_options', 'nklogin/nk-login-settings.php', array($this, 'set_admin_page_template'), 'dashicons-lock', 6  );
    }

    public function set_admin_page_template() {
        include __NKLOGINPATH__.'/views/admin/admin.php';
    }

    //Creating Shortcode for the form
    public static function nk_login_form() {
        ob_start();
        if(!is_user_logged_in()){
            include __NKLOGINPATH__.'/views/public/form.php';
        }
        else {
            include __NKLOGINPATH__.'/views/public/loged.php';
        }
        return ob_get_clean();
    }

    //Register Login Api Route
    public function register_nk_login_route () {

           function nk_post_form_callback (WP_REST_Request $request) {
               $username = $request['username'];
               $password = $request['password'];
               if(empty($username) || empty($password)){
                   return new WP_Error( 'Request failed', '', array( 'status' => 500, "error" => "All fields are required.<br>"));
               }
               else {
                   $attempt = new Nk_Login();
                   $minutes = $attempt->get_remaining_block_minutes();
                   if($attempt::check_if_blocked_ip()){
                       $response = ["status" => "BLOCKED", "message" => "You have been blocked because of to many invalid logins. Please try in {$minutes} minutes."];
                       return $response;
                   }
                   else {
                       $user = wp_authenticate( $username, $password );
                       if(!$user->ID) {
                           $attempt->record_invalid_login($username);
                           return new WP_Error( 'Request failed', '', array( 'status' => 500, "error" => "Invalid login details.<br>"));
                       } else {
                           //cookies for wordpress login
                           wp_set_auth_cookie($user->ID, false);
                           $redirectURl = get_site_url();
                           $response = ["status" => "OK", "redirectURL" => "$redirectURl"];
                           return $response;
                       }
                   }
               }

           }
           register_rest_route( 'nk-login/v1', 'login/?', array(
               'methods' => 'POST',
               'callback' => 'nk_post_form_callback',
           ) );

    }

    //Handle invalid login attempts
    static function record_invalid_login($username) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'nk_invalid_login_attempts';

        $result = self::load_attempts_table($wpdb, $table_name);
        $insert = self::insert_in_attempts_table($wpdb, $table_name, $username, $result);
    }

    static function check_if_blocked_ip() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'nk_invalid_login_attempts';
        $blockedIp = self::load_attempts_table($wpdb, $table_name);
        $blockedTill = $blockedIp[0]['blocked_till'];
        $currentTime = current_time('mysql', 1);

        if($blockedTill  != null && strtotime($currentTime) > strtotime($blockedTill)) {
            $wpdb->delete( $table_name, array('id' => $blockedIp[0]['id']) );
        }

        return ($blockedIp[0]['attempts'] >= 3 && $blockedTill) ? true : false;
    }

    //Get results from attempts table
    static function load_attempts_table ($db, $table) {
        $ip = self::get_client_ip();
        $holder = "%s";
        $array = array();
        $result = $db->get_results ( $db->prepare("SELECT * FROM $table WHERE ip_address = '{$holder}'", $ip),ARRAY_A);
        return $result;
    }

    //Insert data in attempts table
    static function insert_in_attempts_table ($db, $table, $username, $result){
        $ip = self::get_client_ip();
        if(empty($result)){
            $db->insert($table, array(
                'ip_address' => $ip,
                'username' => $username,
                'attempts' => 1,
                'last_attempt' => current_time('mysql', 1)
            ));
            if($db->last_error !== '') :
                $db->print_error();
            else:
                return 1;
            endif;

        } else {
            $numberOfAttempts = $result[0]['attempts'];
            $newAttempt = $numberOfAttempts+1;
            $date = new DateTime();
            $date->modify("+30 minutes");
            $currentTime = $date->format('Y-m-d H:i:s');
            $db->update($table, array( 'attempts' => $newAttempt, 'last_attempt' => current_time('mysql', 1)),array('ip_address'=>$ip));
            if($newAttempt == 3)
                $db->update($table, array( 'blocked_till' => $currentTime),array('ip_address'=>$ip));

            return 2;
        }
    }

    //get client ip
     static function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function get_remaining_block_minutes () {
        global $wpdb;
        $table_name = $wpdb->prefix . 'nk_invalid_login_attempts';
        $result = self::load_attempts_table($wpdb, $table_name);
        $blocked_till = $result[0]['blocked_till'];
        $currentTime = current_time('mysql', 1);

        return round(abs(strtotime($currentTime) - strtotime($blocked_till)) / 60,2);
    }
}
