<?php

/**
 *
 * @link              http://labscreative.com
 * @since             1.0.0
 * @package           Login Form
 *
 * @wordpress-plugin
 * Plugin Name:       WordPress Login Form
 * Plugin URI:        http://labscraetive.com
 * Version:           1.0.0
 * Author:            Nikola Nikoloski
 * Author URI:        http://labscraetive.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nklogin
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

//Defining the plugin path and url
define( '__NKLOGINPATH__', dirname(__FILE__) );
define( '__NKLOGINURL__', plugin_dir_url( __FILE__ ));

// Activation and deactivation actions
function activate_nk_login() {
    require_once __NKLOGINPATH__ . '/inc/class-nk-login-activator.php';
    Nk_Login_Activator::activate();
}

function deactivate_nk_login() {
    require_once __NKLOGINPATH__ . '/inc/class-nk-login-deactivator.php';
    Nk_Login_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_nk_login' );
register_deactivation_hook( __FILE__, 'deactivate_nk_login' );

//import Main class
include(__NKLOGINPATH__.'/inc/class-nk-login.php');

//main class Init
$nkLogin = new Nk_Login();

// Registering the login shortcode
add_shortcode('nk_login_form', array($nkLogin, 'nk_login_form'));
