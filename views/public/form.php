<div id="nk-form">
    <h1>LOGIN </h1>
    <form method="POST" id="nk-login-form" class="login-form">
        <input type="text" name="username" id="username" placeholder="username"/>
        <input type="password" name="password" id="password" placeholder="password"/>
        <button id="login-btn">login</button>
    </form>
    <div id="error-message"></div>
    <div id="loading-animation"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>
</div>
